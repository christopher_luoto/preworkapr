var rolls = 0;
var money = 0;

function collectBid() {
  var startingBet = parseInt(document.getElementById("startingBet").value);
  money += startingBet;
  document.getElementById("stillBet").innerHTML = String(money);
  if (startingBet <= 0) {
    return alert("C'mon, make a real bet")
  } else if (isNaN(startingBet)) {
    return alert("Please make a real bet!")
  } else {
    document.getElementById("table").style.display = "table";
    document.getElementById("play").innerHTML = "Play again?";
  
  }
}

function playGame() {
  var rollsarr = [];
  var moneyarr = [];

  while (money > 0) {
    rollsarr.push(rolls);
    moneyarr.push(money);
    var r1 = Math.floor(6 * Math.random()) + 1;
    var r2 = Math.floor(6 * Math.random()) + 1;
    rolls++;
	
    if ((r1 + r2) === 7) {
      money += 4
    } else {
      money -= 1
    }
  }
  console.log(moneyarr);
  console.log(rollsarr);

  var maxTotal = Math.max(...moneyarr);
  console.log(maxTotal);
  var rollsMax = moneyarr.indexOf(maxTotal) + 1;
  console.log(rollsarr[rollsMax]);

  document.getElementById("maxTotal").innerHTML = String(maxTotal);
  document.getElementById("rollsBroke").innerHTML = String(rollsarr.length);
  document.getElementById("rollsMax").innerHTML = String(rollsMax);

};